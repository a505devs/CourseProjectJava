package ed.sevsu.CourseProject.core.entities;

import java.math.BigDecimal;

public class Product {

    private long id = 0;
    private String name;
    private String sku;
    private int quantity;
    private BigDecimal cost;

    public Product(String name, String sku, int quantity, BigDecimal cost) {
        if (name == null || sku == null || quantity < 0 || cost.doubleValue() < 0 || cost == null) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.sku = sku;
        this.quantity = quantity;
        this.cost = cost;
    }

    public Product(long id, String name, String sku, int quantity, BigDecimal cost) {
        this(name, sku, quantity, cost);
        this.id = id;
    }

    public Product(Product product) {
        this(product.getName(), product.getSku(), product.getQuantity(), product.getCost());
        this.id = product.getId();
    }

    public void setId(long id) {
        if (id == 0 || id < 0) throw new IllegalArgumentException();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        if (name == null || name.equals("")) throw new IllegalArgumentException();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSku(String sku) {
        if (sku == null || sku.equals("")) throw new IllegalArgumentException();
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    public void setCost(BigDecimal cost) {
        if (cost == null || cost.doubleValue() < 0) throw new IllegalArgumentException();
        this.cost = cost;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setQuantity(int quantity) {
        if (quantity < 0) throw new IllegalArgumentException();
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public BigDecimal totalCost() {
        return this.cost.multiply(BigDecimal.valueOf(this.quantity));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && sku.equals(product.sku) && quantity == product.quantity && name.equals(product.name) && cost.equals(product.cost);
    }
}
