package ed.sevsu.CourseProject.configuration;

import ed.sevsu.CourseProject.application.services.ProductService;
import ed.sevsu.CourseProject.core.abstractions.database.ProductRepository;
import ed.sevsu.CourseProject.infrastructure.database.InMemoryProductRepository;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class DependencyInjectionConfiguration {
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ProductRepository productRepository() {
        return new InMemoryProductRepository();
    }
}
