package ed.sevsu.CourseProject.infrastructure.database;

import ed.sevsu.CourseProject.core.abstractions.database.ProductRepository;
import ed.sevsu.CourseProject.core.entities.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InMemoryProductRepository implements ProductRepository {
    List<Product> items = new ArrayList<>();

    public InMemoryProductRepository() {
        items.add(new Product(1, "Apples", "SKU-1138TX", 100, BigDecimal.valueOf(140)));
        items.add(new Product(2, "Cookies", "SKU-1139GG", 250, BigDecimal.valueOf(120)));
        items.add(new Product(3, "Sausages", "SKU-1140PRESSF", 80, BigDecimal.valueOf(395.6)));
    }

    @Override
    public List<Product> getProducts() {
        return items;
    }

    @Override
    public void addProduct(Product product) {
        long nextId = this.items
                .stream()
                .map(Product::getId)
                .max(Long::compare)
                .orElse(0L) + 1;

        product.setId(nextId);
        items.add(product);
    }

    @Override
    public void changeProduct(Product product) {
        items.set(items.indexOf(findById(product.getId())), product);
    }

    @Override
    public Product findById(long id) {
        for (var item : items) {
            if (item.getId() == id) return item;
        }
        return null;
    }
}
