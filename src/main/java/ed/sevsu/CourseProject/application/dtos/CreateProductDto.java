package ed.sevsu.CourseProject.application.dtos;

import java.math.BigDecimal;

public class CreateProductDto {

    private String name;
    private String sku;
    private int quantity;
    private String cost;

    public String getName() {
        return name;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCost() {
        return cost;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
